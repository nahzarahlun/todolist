<?php include 'db.php'; ?>

<?php 

    if(isset($_POST['add_post'])){
        $taks_name = mysqli_real_escape_string($connection,$_POST['taks_name']);
        $query = mysqli_query($connection,"INSERT INTO taks (taks_name,taks_status,taks_date)
                               VALUES ('$taks_name','pending',now()) ");
        header("location: index.php");
    }

    if(isset($_GET['edit'])){
       $taks_id = $_GET['edit'];
        $query = mysqli_query($connection,"UPDATE taks SET taks_status ='selesai' WHERE taks_id='$taks_id'");
        header("location: index.php");
    }

    if(isset($_GET['delete'])){
        $taks_id = $_GET['delete'];
         $query = mysqli_query($connection,"DELETE FROM taks WHERE taks_id='$taks_id'");
         header("location: index.php");
     }

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TODOLIST -APP</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
</head>
<body>
    <div class="contener">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center">TODOLIST-APP</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card shadow-lg border-0">
                    <div class="card-body">
                        <h3>form tambah tugas</h3>
                        <form method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" name="taks_name" placeholder="input nama tugas">
                        </div>
                        <div class="form-group">
                            <button type="submit" name="add_post" class="btn btn-primary btn-block">tambah tugass</button>
                        </div>
                        </form>
                        <h3>list pending tugas</h3>
                        <ul class="list-group">
                            <?php 
                                $query = mysqli_query($connection,"SELECT * FROM taks WHERE taks_status='pending' ");
                                while($row = mysqli_fetch_array($query)){
                                    $taks_id = $row['taks_id'];
                                    $taks_name = $row['taks_name'];

                                
                            ?>
                            <li class="list-group-item">
                                <?php echo $taks_name; ?>
                                <div class="float-right">
                                    <a href="index.php?edit=<?php echo $taks_id?>" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="tandai sudah selesai">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check" viewBox="0 0 16 16">
                                <path d="M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z"/>
                                </svg>
                                </a>
                                <a href="index.php?delete=<?php echo $taks_id?>" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="hapus tugas">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3" viewBox="0 0 16 16">
                                <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5ZM11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H2.506a.58.58 0 0 0-.01 0H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1h-.995a.59.59 0 0 0-.01 0H11Zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5h9.916Zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47ZM8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5Z"/>
                                </svg>
                                </a>
                                </div>
                               
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
            <div class="card shadow-lg border-0">
                    <div class="card-body">
                        <h3>list tugas selesai</h3>
                        <ul class="list-group">
                            <?php 
                                $query = mysqli_query($connection,"SELECT * FROM taks WHERE taks_status='selesai'");
                                while($row = mysqli_fetch_array($query)){

                            
                            ?>
                            <li class="list-group-item">
                                <?php echo $row['taks_name']?>

                                <div class="float-right">
                                    <span class="badge badge-primary"><?php echo $row['taks_status'] ?></span>
                                </div>
                            </li>
                                <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
    <script>

    if(isset($_GET['edit'])){
    $taks_id = $_GET['edit'];
    $query = mysqli_query($connection,"UPDATE taks SET taks_status ='selesai' WHERE taks_id='$taks_id'");
    header("location: index.php");
    }
    </script>



</body>
</html>